$(function () {
    $(window).on('resize', function () {
        var widthWindow = window.innerWidth;
        if (widthWindow < 1025) {
            $('.card-box').not('.slick-initialized').slick();
        } else {
            $('.card-box').filter('.slick-initialized').slick('unslick');
        }
    }).trigger('resize');
});
